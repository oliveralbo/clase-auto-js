
const auto = new Auto();

function armaForm2() {
  let mensaje = document.getElementById("cantidadTitulo");
  let form2 = document.getElementById("form2");

  if (document.getElementById("1").checked) {
    mensaje.innerHTML = "¿Cuantos Km vas a recorrer?";
    form2.children[1].innerHTML = "No avanzar";
    form2.children[3].innerHTML = "Avanzar 5 Km";
    form2.children[5].innerHTML = "Avanzar 10 Km";
  }
  if (document.getElementById("2").checked) {
    mensaje.innerHTML = "¿Cuanto combustible queres cargar?";
    form2.children[1].innerHTML = "No cargar Nafta";
    form2.children[3].innerHTML = "Cargar 5 litros de Nafta ";
    form2.children[5].innerHTML = "Cargar 10 litros de Nafta";
  }
}

function action(form2) {
  let ComponenteMensaje = document.getElementById("respuesta");
  let form1;
  let mensaje;
 

  if (document.getElementById("1").checked) {
    form1 = "recorrer";
  }
  if (document.getElementById("2").checked) {
    form1 = "cargar";
  }

  for (let i = 0; i < form2.length; i++) {
    if (form2[i].checked) {
      if (form1 === "recorrer") {
        mensaje = auto.recorrer(form2[i].value);
      }
      if (form1 === "cargar") {
        mensaje = auto.cargarNafta(form2[i].value);
      }
    }
  }

  ComponenteMensaje.innerHTML = mensaje;
}
