class Auto {
  constructor() {
    this.nafta = 0;
    this.kmRecorridos = 0;
  }

  cargarNafta = (x) => {
    let naftaIngresada = Number(x);
    if (naftaIngresada === 0) {
      return "No cargaste nafta";
    } else if (this.nafta + naftaIngresada > 10) {
      this.nafta = 10;
      return "Tu tanque solo soporta hasta 10Lts de nafta - Se lleno el tanque !";
    } else {
      this.nafta = this.nafta + Number(naftaIngresada);
      return `Se cargaron ${naftaIngresada}Lts de Nafta, ahora tienes ${this.nafta}Lst de nafta`;
    }
  };

  recorrer = (x) => {
    let kmRecorridos = Number(x);
    if (kmRecorridos === 0) {
      return `No avanzaste nada, llevas recorridos ${this.kmRecorridos}Km.`;
    } else if (this.nafta === 0) {
      return "no tenés nafta para andar";
    } else if (kmRecorridos > this.nafta) {
      this.kmRecorridos = this.kmRecorridos + this.nafta;
      let sePudoRecorrer = this.nafta
      this.nafta = 0;
      return `Solo pudiste recorrer ${sePudoRecorrer}km. Te has quedado sin nafta - llevas un total de ${this.kmRecorridos}Km recorridos`;
    } else {
      this.kmRecorridos = this.kmRecorridos + kmRecorridos;
      this.nafta = this.nafta - kmRecorridos;
      return `Recorriste ${kmRecorridos}Km - llevas un total de ${this.kmRecorridos}Km recorridos- Te quedan ${this.nafta}Lts. de nafta`;
    }
  };
}
